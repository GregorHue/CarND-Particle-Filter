#include "particle_filter.h"

#include <math.h>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <string>
#include <vector>

#include "helper_functions.h"

using namespace std;

void ParticleFilter::init(double x, double y, double theta, double std[]) {
 /**
   * TODO: Set the number of particles. Initialize all particles to 
   *   first position (based on estimates of x, y, theta and their uncertainties
   *   from GPS) and all weights to 1. 
   * TODO: Add random Gaussian noise to each particle.
   * NOTE: Consult particle_filter.h for more information about this method 
   *   (and others in this file).
   */
  num_particles = 64; 
  
  default_random_engine gen;
  normal_distribution<double> dist_x(x, std[0]);
  normal_distribution<double> dist_y(y, std[1]);
  normal_distribution<double> dist_theta(theta, std[2]);
  
  for(int i = 0; i < num_particles; i++){    
    Particle p = {
      i,
      dist_x(gen),
      dist_y(gen),
      dist_theta(gen),
      1.0,
      };
    particles.push_back(p);    
  }
  is_initialized = true;
}

void ParticleFilter::prediction(double delta_t, double std_pos[], 
                                double velocity, double yaw_rate) {
  /**
   * TODO: Add measurements to each particle and add random Gaussian noise.
   * NOTE: When adding noise you may find std::normal_distribution 
   *   and std::default_random_engine useful.
   *  http://en.cppreference.com/w/cpp/numeric/random/normal_distribution
   *  http://www.cplusplus.com/reference/random/default_random_engine/
   */
  default_random_engine gen;
  normal_distribution<double> dist_x(0, std_pos[0]);
  normal_distribution<double> dist_y(0, std_pos[1]);
  normal_distribution<double> dist_theta(0, std_pos[2]);
  
  for(Particle& p: particles){
   
    if(abs(yaw_rate) == 0) {
      p.x += (velocity*delta_t*cos(p.theta));
      p.y += (velocity*delta_t*sin(p.theta));
    } else {
      p.x += (velocity/yaw_rate)*(sin(p.theta + yaw_rate*delta_t) - sin(p.theta));
      p.y += (velocity/yaw_rate)*(cos(p.theta) - cos(p.theta + yaw_rate*delta_t));    
      p.theta += yaw_rate*delta_t;
    }
    p.x += dist_x(gen);
    p.y += dist_y(gen);
    p.theta += dist_theta(gen); 
  }
}
  

void ParticleFilter::dataAssociation(vector<LandmarkObs> predicted, 
                                     vector<LandmarkObs>& observations) {
  /**
   * TODO: Find the predicted measurement that is closest to each 
   *   observed measurement and assign the observed measurement to this 
   *   particular landmark.
   * NOTE: this method will NOT be called by the grading code. But you will 
   *   probably find it useful to implement this method and use it as a helper 
   *   during the updateWeights phase.
   */
  for(LandmarkObs& obs: observations){
    double min = numeric_limits<double>::max();    
    for(LandmarkObs& obs_predicted: predicted){
      double distance = dist(obs.x, obs.y, obs_predicted.x, obs_predicted.y);
      if(distance < min){	
        min = distance;
        obs.id = obs_predicted.id;
      }
    }    
  }  
}

void ParticleFilter::updateWeights(double sensor_range, double std_landmark[], 
                                   const vector<LandmarkObs> &observations, 
                                   const Map &map_landmarks) {
  /**
   * TODO: Update the weights of each particle using a mult-variate Gaussian 
   *   distribution. You can read more about this distribution here: 
   *   https://en.wikipedia.org/wiki/Multivariate_normal_distribution
   * NOTE: The observations are given in the VEHICLE'S coordinate system. 
   *   Your particles are located according to the MAP'S coordinate system. 
   *   You will need to transform between the two systems. Keep in mind that
   *   this transformation requires both rotation AND translation (but no scaling).
   *   The following is a good resource for the theory:
   *   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
   *   and the following is a good resource for the actual equation to implement
   *   (look at equation 3.33) http://planning.cs.uiuc.edu/node99.html
   */

  for(Particle& p : particles){    

    vector<LandmarkObs> obs_predicted;    
    for(Map::single_landmark_s landmark : map_landmarks.landmark_list){
      double distance = dist(landmark.x_f, landmark.y_f, p.x, p.y);      
      if(distance < sensor_range){	
        LandmarkObs obs = {
          landmark.id_i,
          landmark.x_f,
          landmark.y_f
        };
        obs_predicted.push_back(obs);
      }
    }
        
    //Transformation 
    vector<LandmarkObs> observations_transformed;    
    for(LandmarkObs obs : observations){
      LandmarkObs observation_transformed = {
        obs.id,
        // Rotation (by -theta) and translation of the coordinates frame
        obs.x*cos(-p.theta) + obs.y*sin(-p.theta) + p.x,
        -obs.x*sin(-p.theta) + obs.y*cos(-p.theta) + p.y,    
      };
      observations_transformed.push_back(observation_transformed);      
    }
    
    dataAssociation(obs_predicted, observations_transformed);   
    
    vector<int> associations;
    vector<double> sense_x;
    vector<double> sense_y;
    
    double weight = 1;
    
    for(LandmarkObs& obs_transformed : observations_transformed){
      // index begins at 0
      Map::single_landmark_s m =  map_landmarks.landmark_list[obs_transformed.id -1];
      double numerator = exp(-((pow(obs_transformed.x - m.x_f, 2))/(2.0*std_landmark[0]*std_landmark[0]) + (pow(obs_transformed.y - m.y_f, 2))/(2.0*std_landmark[1]*std_landmark[1])));
      double prob = numerator/(2.0*M_PI*std_landmark[0]*std_landmark[1]);
      
      weight *= prob;
      
      associations.push_back(obs_transformed.id);
      sense_x.push_back(obs_transformed.x);
      sense_y.push_back(obs_transformed.y);      
    }
    p.weight = weight;
    weights.push_back(weight); 
    SetAssociations(p, associations, sense_x, sense_y);
  }
}

void ParticleFilter::resample() {
  /**
   * TODO: Resample particles with replacement with probability proportional 
   *   to their weight. 
   * NOTE: You may find std::discrete_distribution helpful here.
   *   http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution
   */
  
  vector<Particle> particles_temp;
  default_random_engine gen;
  discrete_distribution<> d(weights.begin(), weights.end());
  
  int index;
  for(int i = 0; i < num_particles; i++){      
    index = d(gen);  
    particles_temp.push_back(particles[index]);
  }
  weights.clear();
  particles = particles_temp;
}

void ParticleFilter::SetAssociations(Particle& particle, 
                                     const vector<int>& associations, 
                                     const vector<double>& sense_x, 
                                     const vector<double>& sense_y) {
  // particle: the particle to which assign each listed association, 
  //   and association's (x,y) world coordinates mapping
  // associations: The landmark id that goes along with each listed association
  // sense_x: the associations x mapping already converted to world coordinates
  // sense_y: the associations y mapping already converted to world coordinates
  particle.associations= associations;
  particle.sense_x = sense_x;
  particle.sense_y = sense_y;
}

string ParticleFilter::getAssociations(Particle best) {
  vector<int> v = best.associations;
  std::stringstream ss;
  copy(v.begin(), v.end(), std::ostream_iterator<int>(ss, " "));
  string s = ss.str();
  s = s.substr(0, s.length()-1);  // get rid of the trailing space
  return s;
}

string ParticleFilter::getSenseCoord(Particle best, string coord) {
  vector<double> v;

  if (coord == "X") {
    v = best.sense_x;
  } else {
    v = best.sense_y;
  }

  std::stringstream ss;
  copy(v.begin(), v.end(), std::ostream_iterator<float>(ss, " "));
  string s = ss.str();
  s = s.substr(0, s.length()-1);  // get rid of the trailing space
  return s;
}